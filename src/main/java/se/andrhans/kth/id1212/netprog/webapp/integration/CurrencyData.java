package se.andrhans.kth.id1212.netprog.webapp.integration;

import se.andrhans.kth.id1212.netprog.webapp.model.Currency;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Interface to access the database.
 */
public interface CurrencyData extends CrudRepository<Currency, Long> {
    Currency findByCode(@Param("code") String code);
    List<Currency> findAllByOrderByCodeAsc();
}
