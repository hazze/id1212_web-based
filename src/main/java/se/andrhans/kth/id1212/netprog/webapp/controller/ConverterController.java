package se.andrhans.kth.id1212.netprog.webapp.controller;

import se.andrhans.kth.id1212.netprog.webapp.integration.CurrencyData;
import se.andrhans.kth.id1212.netprog.webapp.model.Conversion;
import se.andrhans.kth.id1212.netprog.webapp.model.Currency;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * Controller for the converter.
 */
@Controller
public class ConverterController {

    private final CurrencyData currencyData;

    private Iterable<Currency> currencies;
    private Conversion conversion;

    /**
     * Constructor for the ConverterController
     * @param currencyData The current currency-repository in the database.
     */
    @Autowired
    public ConverterController(CurrencyData currencyData) {
        this.currencyData = currencyData;
        this.currencies = currencyData.findAllByOrderByCodeAsc();
        this.conversion = new Conversion("EUR", "SEK", 0, 0);
    }

    /**
     * The function called when the requested url is root.
     * @param model The model to map.
     * @return Returns what html file to give to the client-view.
     */
    @GetMapping(value = "/")
    public String converter(Model model) {
        model.addAttribute("currencies", currencies);
        if (!model.containsAttribute("conversion"))
            model.addAttribute("conversion", conversion);
        return "converter";
    }

    /**
     * The function called from the from->action event, converts the
     * input and returns to root with a redirect.
     * @param conversion The conversion to convert.
     * @return Redirect back to root url.
     */
    @PostMapping(value = "/convert")
    public String convert(@ModelAttribute Conversion conversion) {
        if (conversion.getFromCurrency().equals(conversion.getToCurrency())) {
            this.conversion = conversion;
            return "redirect:/";
        }

        this.conversion = new Conversion(conversion.getFromCurrency(), conversion.getToCurrency(), conversion.getAmount(), convertCurrencies(conversion));
        return "redirect:/";
    }

    /**
     * Resets the conversion object and therefore the view.
     * @return Redirect back to root url.
     */
    @GetMapping(value = "/reset")
    public String reset() {
        this.conversion = new Conversion("EUR", "SEK", 0, 0);
        return "redirect:/";
    }

    private double convertCurrencies(Conversion conversion) {
        Currency fromCurrency = currencyData.findByCode(conversion.getFromCurrency());
        Currency toCurrency = currencyData.findByCode(conversion.getToCurrency());
        double inEuro = conversion.getAmount() * (1 / fromCurrency.getRate());
        return (double) Math.round(inEuro * toCurrency.getRate() * 100d) / 100d;
    }
}
