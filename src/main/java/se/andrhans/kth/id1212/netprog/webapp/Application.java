package se.andrhans.kth.id1212.netprog.webapp;

import se.andrhans.kth.id1212.netprog.webapp.integration.CurrencyData;
import se.andrhans.kth.id1212.netprog.webapp.model.Currency;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * The class for the application itself. Contains the main.
 */
@SpringBootApplication
public class Application implements CommandLineRunner {

    private final CurrencyData currencyData;

    /**
     * The constructor for the application.
     * @param currencyData The current database to access for data.
     */
    @Autowired
    public Application(CurrencyData currencyData) {
        this.currencyData = currencyData;
    }

    /**
     * Main function.
     * @param args Args.
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    /**
     * The run function for the application. Inserts all data to the database.
     * @param strings Arguments.
     * @throws Exception Possible exception from the file read.
     */
    @Override
    public void run(String... strings) throws Exception {
        insertCurrencyData();
    }

    private void insertCurrencyData() throws IOException {
        currencyData.deleteAll();
        String content = new String(Files.readAllBytes(Paths.get("/Users/hazze/java/net_prog/homework4/src/main/resources/static/json/rate_data.json")));
        JSONObject json = new JSONObject(content);
        JSONObject rates = json.getJSONObject("rates");
        for (String key: rates.keySet()) {
            currencyData.save(new Currency(key ,rates.getDouble(key)));
        }
    }
}
